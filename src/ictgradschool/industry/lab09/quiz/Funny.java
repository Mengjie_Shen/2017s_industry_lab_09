package ictgradschool.industry.lab09.quiz;

import org.omg.CORBA.PUBLIC_MEMBER;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

/**
 * Created by mshe666 on 4/12/2017.
 */
public class Funny {
    public static void main(String[] args) {
        Funny f = new Funny();
        f.start();
    }

    public void start() {
        ArrayList list = new ArrayList();
        Character letter = new Character('a');
        list.add(letter);
        if (list.get(0).equals("a")) {
            System.out.println("funny");
        } else {
            System.out.println("Not funny");
        }


        ArrayList<Point> list2 = new ArrayList<Point>();
        Point pt1 = new Point(3, 4);
        list2.add(pt1);
        Point pt2 = list2.get(0);
        pt2.x = 23;
        if (pt2 == pt1) {
            System.out.println("Same object");
        } else {
            System.out.println("Different object");
        }

        ArrayList list3 = new ArrayList();
        list3.add('a');
        list3.add(0, 34);
//        String c1 = (String) list3.get(1);
        String c1 = String.valueOf(list3.get(1));

        ArrayList<String> list4 = new ArrayList();
        list4.add("Hello");
        list4.add("World");

        for (String s : list4) {
            System.out.println(s);
        }

        ArrayList<String> list5 = new ArrayList();

        //simple for loop
//        list5 = newArray1(list4);


        //advanced for loop
        list5 = newArray2(list4);

        //iterator
//        list5 = newArray3(list4);


        for (String s : list5) {
            System.out.println(s);
        }


    }

    public ArrayList<String> newArray1(ArrayList<String> list) {
        ArrayList<String> newList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            String lowerStr = list.get(i).toLowerCase();
            list.set(i, lowerStr);
        }
        return newList;
    }

    public ArrayList<String> newArray2(ArrayList<String> list) {
        ArrayList<String> newList = new ArrayList<>();
        for (String s : list) {
            s = s.toLowerCase();
//            System.out.println(s);
            newList.add(s);
        }
        return newList;
    }

    public ArrayList<String> newArray3(ArrayList<String> list) {
        ListIterator<String> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            String lowerStr = listIterator.next().toLowerCase();
            listIterator.set(lowerStr);
        }
        return list;
    }
}
